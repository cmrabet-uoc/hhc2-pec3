/**
 * Herramientas HTML y CSS II - PEC 2.
 * Chakir Mrabet, 2020.
 * Entry script.
 */

/**
 * This is to fix the issue explained here: https://flaviocopes.com/parcel-regeneratorruntime-not-defined/
 * This is an issue with Parcel.
 */

import "regenerator-runtime/runtime";

/**
 * Custom made modules.
 */

import nav from "./nav";
import video from "./video.js";

/**
 * Navbar functionality.
 */

nav();

/**
 * Video Control
 */

video();
