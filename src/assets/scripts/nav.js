/**
 * Herramientas HTML y CSS II - PEC 2.
 * Chakir Mrabet, 2020.
 * Navbar menu selection script.
 */

/**
 * Navbar functionality.
 *
 * 1. Sets the active menu link in the navbar.
 * 2. Gets the current URL from the browser and compares it with
 *    the HREF value of each link in the navbar menu. When it finds
 *    a match, it sets its class to "active".
 */

export default function NavbarMenuControl() {
  const urlPath = window.location.origin + window.location.pathname;
  const appbarMenuItems = document.querySelectorAll(".js-navbar-menu li");

  appbarMenuItems.forEach((li) => {
    const a = li.querySelector("a");

    if (a) {
      if (a.href === urlPath) {
        li.classList.add("menu__item--is-selected");
        return;
      }
      li.classList.remove("menu__item--is-selected");
    }
  });
}
