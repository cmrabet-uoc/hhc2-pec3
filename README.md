# HTML and CSS Tools II

## Project 3 - Universitat Oberta de Catalunya

[Chakir Mrabet](mailto:cmrabet@gmail.com) - December 31, 2020,

# What is it?

This project consists of two pages taken from the baroque festival web made in PEC2 that have been refactored using the [TailwindCSS](https://tailwindcss.com/) utility library. The pages that have been refactored are _Home_ and _Artists_.

The project has been implemented using Parcel, Bootstrap, SASS, Stylelint, and the [H.Roberts style user guide](https://cssguidelin.es/).

# Where is it?

You can visit the site [here](https://lully-festival-twcss.netlify.app/).

# Instructions

For development, run the following command from the root folder:

`npm run dev`

For production:

`npm run build`

The files for production will be in the `dist` folder.
