module.exports = {
  extends: "stylelint-config-standard",
  plugins: [
    "stylelint-order",
    "stylelint-scss",
    "stylelint-selector-bem-pattern",
  ],
  rules: {
    indentation: 2,
    "max-line-length": 110,
    // This is to allow the type of comments that H.Robert uses.
    "comment-whitespace-inside": null,
    "at-rule-no-unknown": [
      true,
      {
        ignoreAtRules: [
          "tailwind",
          "layer",
          "theme",
          "apply",
          "responsive",
          "variants",
          "screen",
          "use",
          // For Sass.
          "include",
          "mixin",
          "function",
          "if",
          "else",
          "return",
          "for",
        ],
      },
    ],
    // Sort control.
    "order/properties-order": ["width", "height"],
    // Sass
    "selector-nested-pattern": "^&",
    "no-descending-specificity": null,
    "no-eol-whitespace": null,
    "declaration-empty-line-before": null,
    "value-keyword-case": null,

    // BEM
    "plugin/selector-bem-pattern": {
      componentName: "[A-Z]+",
      componentSelectors: {
        initial: "^\\.{componentName}(?:-[a-z]+)?$",
        combined: "^\\.combined-{componentName}-[a-z]+$",
      },
      utilitySelectors: "^\\.util-[a-z]+$",
    },
  },
};
