module.exports = {
  purge: ["./src/**/*.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    // Puntos de ruptura propios con  nombres semánticos.
    screens: {
      phone: "576px",
      tablet: "720px",
      computer: "1024px",
      wide: "1200px",
    },
    // Fuentes de Google Fonts.
    fontFamily: {
      serif: ["EB Garamond", "serif"],
      sans: ["Oswald", "sans-serif"],
    },
    // Extiendo Tailwind añadiendo mis propios estilos.
    extend: {
      // Colores con nombres semánticos.
      colors: {
        primary: {
          lighter: "rgba(255, 255, 255, 0.5)",
          light: "rgb(144, 106, 123)",
          shadow: "rgba(161,128,143,.5)",
          DEFAULT: "rgb(77, 38, 53)",
          dark: "rgb(45, 23, 31)",
        },
        secondary: {
          light: "rgb(108, 117, 125)",
          DEFAULT: "rgb(33, 37, 41)",
          dark: "rgb(10,10,10)",
        },
      },
      // El mínimo borde con el que viene Tailwind is 2px..
      borderWidth: {
        1: "1px",
      },
      fontSize: {
        "4.5xl": "2.65rem",
      },
      // Esto es para el banner, desplazo el fondo en pantallas grandes.
      backgroundPosition: {
        neg180: "-180px",
      },
      // Plantillas grid propias, Tailwind usa por defecto minmax.
      gridTemplateRows: {
        "4Flex": "repeat(4, 1fr)",
      },
      gridTemplateColumns: {
        "4Flex": "repeat(4, 1fr)",
      },
    },
  },
};
